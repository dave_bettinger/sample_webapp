app_name = 'sample_webapp'
app_config = node[app_name]

include_recipe "apache2"

# Set up the Apache virtual host 
web_app app_name do
  server_name app_config['server_name']
  docroot app_config['docroot']
  #server_aliases [node['fqdn'], node['hostname']]
  template "#{app_name}.conf.erb"
  log_dir node['apache']['log_dir']
end

template '/var/www/index.html' do
  source 'index.html.erb'
  mode '0755'
  owner 'root'
  group 'root'
  variables({:name => app_name})
end
