name             'sample_webapp'
maintainer       'demo'
maintainer_email 'demo'
license          'All rights reserved'
description      'Installs/Configures sample_webapp'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'apache2'
